# Table of Contents
* [Introduction](#introduction)

## Introduction
In this section we will cover how to build the base operating system. Please consult the *[Table of Contents](#table-of-contents)* to navigate to a specific section, or read the next sub-section to begin!

### What is the Base Operating System?
In this section we will be building the software that makes up the core of DULOS. The core software that we will be building is what is referred to as the *base operating system*. While what is or is not considered to be part of a base operating system is still highly debated DULOS considers the following to be part of the base operating system.

1. A POSIX compliant set of core utilities (this does not including non-script centric utilities such as `ls`)
2. An ANSI compliant C compiler
3. A POSIX compliant C library
4. An ANSI compliant common lisp REPL
5. A POSIX compliant init system and daemon process management utility set
6. A POSIX compliant bootloader
7. A POSIX compliant kernel
8. A kernel utilities set of your choice
9. An audio server of your choice
10. A package manager of your choice

This may not *seem* like much and that is on purpose. The whole point of DULOS is to only require the bare minimum so that you, the **actual user**, can decide what you want to use. There is no point in telling you that you need to use the `ls` program when no script **should be using ls**. Something like `ls` is an interactive tool that **you** use, so if you want to never install `ls` and just install something like `exa` instead then you should be able to. As long as you have the required compliant software to build and run compliant programs, then you are good to go.

*Read more about what a base operating system is [here](insert-documentation-link-here).*

### What Are the Steps to Building the Base Operating System?
The steps to building the base operating system, while tedious and long, are not *too* complicated once you have done them once or twice. For the purposes of DULOS these steps are actually heavily trivialized down to running a few commands and then waiting for them to finish. To put it simply the steps to building the base operating system for DULOS are as follows.

1. [Installing GNUX Guix into the Live Environment](#installing-gnu-guix-into-the-live-environment)
2. [Building Temporary Tools with GNU Guix](#building-temporary-tools-with-gnu-guix)
3. [Chrooting into DULOS](#chrooting-into-dulos)
4. [Installing GNU Guix into DULOS](#installing-gnu-guix-into-dulos)
4. [Installing Needed Software with GNU Guix](#installing-needed-software-with-gnu-guix)
5. [Building Needed Software From Source](#building-needed-software-from-source)

With that said it is important to note that if you want to change out software do so as you come across the software you want replace in each step. With that out of the way lets get started! Please continue onto the next sub-section: [Installing GNUX Guix into the Live Environment](#installing-gnu-guix-into-the-live-environment).

## Installing GNUX Guix into the Live Environment
The very first thing we will need to do is install the package manager we will be using to build our temporary tools into our live environment. The package manager will be using to build the aformentioned temporary tools, and later as the package manager in DULOS, is the GNU guix package manager. GNU guix is an advanced functional package manager written in Guile Scheme, a dialect of Lisp. This package fits perfectly into what DULOS wants to do as it is a programmable package manager, written in lisp, that does one thing and does it well. In order to install GNU guix into our live environment we will first need to create a directory to build GNU guix in.
```
mkdir -p "${DULOS_BUILD}"/guix
cd "${DULOS_BUILD}"/guix
```
Next we will need to pull the installation script for GNU guix and then run it to do the installation.
```
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
./guix-install
```
When prompted answer yes. After the script finishes we will need to update GNU guix to the latest version by running the following.
```
guix pull
```
This command may take some time so finish feel free to take a moment to walk around. Once the command finishes we should have an up-to-date version of GNU guix which means we are ready to start building our temporary tools. Please continue onto the next sub-section: [Building Temporary Tools with GNU Guix](#building-temporary-tools-with-gnu-guix).

## Building Temporary Tools with GNU Guix
In order for us to build our base operating system we will need some temporary tools. Luckily for us there are not too many temporary tools that we will need to build and GNU guix makes building them very easy. In the following command we will be building a toolchain, coreutilities, libselinux, gzip, xz, and tar.
```
mkdir tmp && cd tmp
guix pack -S /temp-tools=bin gcc-toolchain coreutils libselinux gzip xz tar git
```
Now all we have to do is install these needed tools to a temporary directory on DULOS.
```
mv *.tar.gz "${DULOS_ROOT}"/opt
cd "${DULOS_ROOT}"/opt
tar -xvf *.tar.gz && rm -f *.tar.gz
```
Now that we have that done we can move onto chrooting into DULOS. Move onto the next sub-section: [Chrooting into DULOS](#chrooting-into-dulos).

## Chrooting into DULOS
Before we actually `chroot` into DULOS we need to move over a few configuration files that we will need when building our base operating system.
```
mkdir -p "${DULOS_ROOT}"/usr/src/source-code/operating-system/busybox
mkdir -p "${DULOS_ROOT}"/usr/src/source-code/operating-system/linux
cp "${DULOS_CONFIGS}"/section-02/DULOS-BusyBox.config "${DULOS_ROOT}"/usr/src/source-code/operating-system/.config
cp "${DULOS_CONFIGS}"/section-02/DULOS-linux_x86.config "${DULOS_ROOT}"/usr/src/source-code/operating-system/linux/.config
```
Now we can `chroot` into DULOS. All we need to do is `chroot` into `DULOS_ROOT` specifying our shell and our `PATH`.
```
chroot "${DULOS_ROOT}" /opt/temp-tools/env -i PATH=/opt/temp-tools/ /opt/temp-tools/bash --login
```
Now we are in the DULOS system and can start building our base operating system. Please continue onto the next sub-section: [Installing GNU Guix into DULOS](#installing-gnu-guix-into-dulos).

## Installing GNU Guix into DULOS
The very first thing we need to do is install the GNU package manager into DULOS. The steps are the exact same as before. First need to create a directory to build GNU guix in.
```
mkdir -p /usr/src/source-code/operating-system/guix
cd /usr/src/source-code/operating-system/guix
```
Next we will need to pull the installation script for GNU guix and then run it to do the installation.
```
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
./guix-install
```
When prompted answer yes. After the script finishes we will need to update GNU guix to the latest version by running the following.
```
guix pull
```
This command may take some time so finish feel free to take a moment to walk around. Once the command finishes we should have an up-to-date version of GNU guix which means we are ready to start building the rest of our base operating system. Please continue onto the next sub-section: [Installing Needed Software with GNU Guix](#installing-needed-software-with-gnu-guix).

## Installing Needed Software with GNU Guix
Now we can start building the software that GNU guix will manage from source. Run the following to build our C/C++ toolchain, the SELinux libraries, our Common Lisp REPL, an audio server, and our bootloader.
```
guix build gcc-toolchain libselinux sbcl pipwire syslinux
```
This command can take awhile as it is building the above software from source; therefore it may be a good idea to take a break. Once the command does finish we can move onto building the base software that GNU guix will not be managing. Please continue onto the next sub-section: [Building Needed Software From Source](#building-needed-software-from-source).

## Building Needed Software From Source
In this section we will be building a few pieces of software that GNU guix will not be managing. There is not too many pieces of software to build ourselves, but it is easier to manage our init system, daemon process utilities, kernel, and coreutilities if we build them on our own. The first piece of software we are going to build is our initialization system, `sinit`; this process is rather straight-forward.
```
cd /usr/src/source-code/operating-system
git clone git://git.suckless.org/sinit
cd sinit
make
make DESTDIR=/usr/local/sbin install
make clean
```
Next we need to build our process management utilities.
```
cd ../
wget http://untroubled.org/daemontools-encore/daemontools-encore-1.10.tar.gz
tar -xvf daemontools-encore-1.10.tar.gz
cd daemontools-encore-1.10
make
make DESTDIR=/usr/local/sbin install
make clean
```
Next, we need to build our coreutilities.
```
cd ../
git clone git://git.busybox.net/busybox busybox/busybox-release
cp busybox/.config busybox/busybox-release
cd busybox/busybox-release
make
make DESTDIR=/usr/local/bin install
make clean
```
Lastly it is time to build our kernel.
```
cd ../
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.19.1.tar.xz
xz -d linux-5.19.1.tar.xz
tar -xvf linux-5.19.1.tar
mv linux-5.19.1 linux
cd linux && cp .config linux-5.19.1
cd linux-5.19.1
make
make modules_install
make install
make clean
```
Fantastic, we have finished building our base operating system and are ready to start building out our userland. Move onto the next section: [Building the Userland](https://gitlab.com/FOSSilized_Daemon/nexus/-/blob/master/doc/guides/nexus-for-pcs/sections/section-03_building_the_userland.md).
