# Table of Contents

## Introduction
This section will cover how to configure the system disk. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the next sub-section to begin!

### What is System Configuration?
This section covers *system configuration*, by this we mean configuring standard system files and kernel specific files (such as `/etc/fstab`, `/etc/hostname`, etc.) and not files whose configuration is dependent on certain programs (such as `/etc/inittab`). Without further ado, lets get started.

### Things to Note
This section is entirely focused on editing a lot of configuration files. Some steps may simply tell you to add certain items to a file and then provide you with the exact contents to add, therefore it is up to you to decide whether or not you want to use a text editor or use something like `cat` and `sed`.

## Configuring /swap
While many systems use a swap partition DULOS has taken to the new trend of a swap file as it is easier to create, maintain, and remove. The process of creating a swap file is rather simple, using the `dd` and `mkswap` programs we can rather quickly create our swap file with *1G* of memory.
```
dd if=/dev/zero of="${DULOS_ROOT}"/swap bs=1024 count=1048576
mkswap "${DULOS_ROOT}"/swap
```
Now we can move onto the next sub-section: [Configuring /etc/fstab](#configuring-etc-fstab).

## Configuring /etc/fstab
The `/etc/fstab` file is the operating systems file system table. It tells the operating system what devices to mount automatically and where to mount them. Aside from just mounting required devices we can also use `/etc/fstab` to mount external devices automatically after we plug them into our computer. In this sub-section we will be configuring a proper and clean `/etc/fstab` file, using UUIDs, that can be easily maintainable for a long-term system.

The reason we are using UUIDs is because if we just use a device name like `/dev/sda` and have two devices mounted there is a chance some issue could occur causing them to be mounted in the wrong order; thus drive two could become `/dev/sda` instead of `/dev/sdb`. These steps are dependent on whether your computer uses BIOS or UEFI boot, so follow the section corresponding to your needs.

* [Configuring /etc/fstab (BIOS)](#configuring-etc-fstab-bios)
* [Configuring /etc/fstab (UEFI)](#configuring-etc-fstab-uefi)

### Configuring /etc/fstab (BIOS)
In order to configure the `/etc/fstab` file we will have to find the UUIDs of our partitions and then set how they should be mounted in the `/etc/fstab` file. This sub-section takes two steps:

1. [Finding the UUIDs of Our Partitions](#finding-the-uuids-of-our-partitions)
2. [Adding Our Partitions to /etc/fstab](#adding-our-partitions-to-etc-fstab)

#### Finding the UUIDs of Our Partitions
The process of finding the UUIDs if partitions is a rather easy one, it does not take long. Using the `blkid` program we will save the UUIDs of our partitions to a set of variables.
```
BOOT_UUID=$(blkid | grep "boot" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
ROOT_UUID=$(blkid | grep "root" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
HOME_UUID=$(blkid | grep "home" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
USR_UUID=$(blkid | grep "usr" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
OPT_UUID=$(blkid | grep "opt" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
VAR_UUID=$(blkid | grep "var" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
VARTMP_UUID=$(blkid | grep "vartmp" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
TMP_UUID=$(blkid | grep "tmp" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
SRV_UUID=$(blkid | grep "srv" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
```
With those saved to environment variables it is time to move onto the next step: [Adding Our Partitions to /etc/fstab](#adding-our-partitions-to-etc-fstab).

#### Adding Our Partitions to /etc/fstab
Now we will add our partitions to the `/etc/fstab` file. As this guide aims to create an operating system for long-term use, we will format our `/etc/fstab` to be clean and easy to read. Do note that there are 36 spaces between the UUID variables and mountpoints as well as 40 spaces between options and dump/pass. Add the following to `"${DULOS_ROOT}"/etc/fstab`.
```
#################
# System Drives #
#################
# <UUID>                                    <mountpoint>    <type>    <options>                                         <dump/pass>
UUID="${ROOT_UUID}"                          /               ext4      noatime,nouser,ro                                 0    1
UUID="${BOOT_UUID}"                          /boot           ext4      noauto,nouser,noatime,ro                          1    2
UUID="${USR_UUID}"                           /usr            ext4      nodev,nouser,noatime,ro                           0    3
UUID="${OPT_UUID}"                           /opt            ext4      nodev,nouser,noatime,ro                           0    3
UUID="${VAR_UUID}"                           /var            ext4      nodev,nouser,noexec                               0    3
UUID="${VARTMP_UUID}"                        /var/tmp        ext4      nodev,nouser,noatime                              0    3
UUID="${TMP_UUID}"                           /tmp            ext4      nodev,nouser,noatime,noexec                       0    3
UUID="${SRVUUID}"                            /srv            ext4      nodev,nouser,noatime,noexec                       0    3
UUID="${HOMEUUID}"                           /home           ext4      nodev,nouser,noatime                              0    3
/swap                                        swap            swap      defaults                                          0    0

###################
# External Drives #
###################
# <UUID>                                    <mountpoint>    <type>    <options>                                         <dump/pass>
```
Fantastic, now move onto the next sub-section: [Configuring /etc/passwd](#configuring-etc-passwd).

### Configuring /etc/fstab (UEFI)
In order to configure the `/etc/fstab` file we will have to find the UUIDs of our partitions and then set how they should be mounted in the `/etc/fstab` file. This sub-section takes two steps:

1. [Finding the UUIDs of Our Partitions](#finding-the-uuids-of-our-partitions)
2. [Adding Our Partitions to /etc/fstab](#adding-our-partitions-to-etc-fstab)

#### Finding the UUIDs of Our Partitions
The process of finding the UUIDs if partitions is a rather easy one, it does not take long. Using the `blkid` program we will save the UUIDs of our partitions to a set of variables.
```
BOOT_UUID=$(blkid | grep "boot" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
EFI_UUID=$(blkid | grep "efi" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
ROOT_UUID=$(blkid | grep "root" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
HOME_UUID=$(blkid | grep "home" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
USR_UUID=$(blkid | grep "usr" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
OPT_UUID=$(blkid | grep "opt" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
VAR_UUID=$(blkid | grep "var" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
VARTMP_UUID=$(blkid | grep "vartmp" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
TMP_UUID=$(blkid | grep "tmp" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
SRV_UUID=$(blkid | grep "srv" | awk '{print $3}' | sed 's/UUID=//g;s/"//g')
```
With those saved to environment variables it is time to move onto the next step: [Adding Our Partitions to /etc/fstab](#adding-our-partitions-to-etc-fstab).

#### Adding Our Partitions to /etc/fstab
Now we will add our partitions to the `/etc/fstab` file. As this guide aims to create an operating system for long-term use, we will format our `/etc/fstab` to be clean and easy to read. Do note that there are 36 spaces between the UUID variables and mountpoints as well as 40 spaces between options and dump/pass. Add the following to `"${DULOS_ROOT}"/etc/fstab`.

```
#################
# System Drives #
#################
# <UUID>                                    <mountpoint>    <type>    <options>                                         <dump/pass>
UUID="${ROOT_UUID}"                          /               ext4      noatime,nouser,ro                                 0    1
UUID="${BOOT_UUID}"                          /boot           ext4      noauto,nouser,noatime,ro                          1    2
UUID="${EFI_UUID}"                           /boot/efi       vfat      noauto,nouser,noatime,ro                          1    2
UUID="${USR_UUID}"                           /usr            ext4      nodev,nouser,noatime,ro                           0    3
UUID="${OPT_UUID}"                           /opt            ext4      nodev,nouser,noatime,ro                           0    3
UUID="${VAR_UUID}"                           /var            ext4      nodev,nouser,noexec                               0    3
UUID="${VARTMP_UUID}"                        /var/tmp        ext4      nodev,nouser,noatime                              0    3
UUID="${TMP_UUID}"                           /tmp            ext4      nodev,nouser,noatime,noexec                       0    3
UUID="${SRVUUID}"                            /srv            ext4      nodev,nouser,noatime,noexec                       0    3
UUID="${HOMEUUID}"                           /home           ext4      nodev,nouser,noatime                              0    3
/swap                                        swap            swap      defaults                                          0    0

###################
# External Drives #
###################
# <UUID>                                    <mountpoint>    <type>    <options>                                         <dump/pass>
```
Fantastic, now move onto the next sub-section: [Configuring /etc/passwd](#configuring-etc-passwd).

## Configuring /etc/passwd
The `/etc/passwd` file contains information on users and groups. In this section we will simply add the root user to the `/etc/passwd` file.
```
echo "root::0:0:root:/root:/bin/sh" > "${DULOS_ROOT}"/etc/passwd
```
Fantastic, now move onto the next sub-section: [Configuring /etc/group](#configuring-etc-group).

## Configuring /etc/group
In this file we configure our groups. The `/etc/group` file contains systems information on what groups exist and their corresponding numeric value. As we are building a system from scratch we will be adding only the required groups for now. Add the following to `"${DULOS_ROOT}"/etc/groups`.
```
root:x:0:
bin:x:1:
sys:x:2:
kmem:x:3:
tty:x:4:
daemon:x:5:
disk:x:6:
dialout:x:7:
video:x:8:
utemp:x:9:
usb:x:10:
floppy:x:11:
mtp:x:12:
cdrom:x:13:
wheel:x:14:
```
Fantastic, now move onto the next sub-section: [Configuring /etc/profile](#configuring-etc-profile).

## Configuring /etc/profile
The `/etc/profile` is a configuration script which is run by default on login shells, unless a `"${HOME}"/.profile` is found (shell depending). In this sub-section we will fully configure a default `/etc/profile`. Add the following to `"${DULOS_ROOT}"/etc/profile`.
```
################################
# Handle Environment Variables #
################################
set_environment_variables() {
	# Directories containing installed programs.
	export PATH=/usr/bin:/usr/sbin:"${HOME}"/.local/bin:/usr/local/bin:/usr/local/sbin

	# Language settings.
	export LC_TYPE="en_US.UTF-8"
	export LANG=en_US.UTF-8

	if command -v id > /dev/null 2>&1; then
		export USER=$(id -un)
	fi

	if command -v less > /dev/null 2>&1; then
		export PAGER=less
	fi

	if command -v emacs > /dev/null 2>&1; then
		export VISUAL='emacs -nw'
		export EDITOR='emacs -nw'
	fi
}

###########################
# Handle General Settings #
###########################
set_general_settings() {
	umask 077
}

########
# Main #
########
init_profile() {
	set_environment_variables
	set_general_settings
}

init_profile
```
Fantastic, now move onto the next sub-section: [Configuring /etc/hostname](#configuring-etc-hostname).

## Configuring /etc/hostname
The `/etc/hostname` contains our systems hostname. In order to retain some privacy we will be setting our hostname to the most popular one used.
```
echo "host" > "${DULOS_ROOT}"/etc/hostname
```
Fantastic, now move onto the next sub-section: [Configuring /etc/issue](#configuring-etc-issue).

## Configuring /etc/issue
In this sub-section we will be configuring the `/etc/issue` file which is displayed when we login into the system via a tty. We will configure the system to show the message *Welcome to DULOS powered by Linux (Version xxx.xxx.xxx)*.
```
echo "Welcome to DULOS powered by Linux (Version \r)" > "${DULOS_ROOT}"/etc/issue
```
Fantastic, now move onto the next sub-section:
