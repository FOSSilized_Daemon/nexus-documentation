# Table of Contents
* Introduction
	* What is Building the Base Operating System?
	* What Software Makes Up the Base of DULOS?
	* What Are the Steps to Building the Base Operating System?

## Introduction
In this section we will cover how to build the base operating system. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the next sub-section to begin!

### What is Building the Base Operating System?
In this section we will building all of the software that makes up our base operating system; in other words the software that is considered standard by this project. This section will take quite awhile so it is encouraged that you plug your computer into a charger and be ready to be busy for awhile!

*Read more about what a base operating system is [here](insert-documentation-link-here).*

### What Software Makes Up the Base of DULOS?
While DULOS is meant to be a minimalist distribution and thus only the following are considered part of the base operating system:

1. A POSIX compliant set of core utilities (not including non-script centric utilities such as `ls`)
2. An ANSI compliant C compiler
3. A POSIX compliant C library
4. An ANSI compliant common lisp REPL
5. A POSIX compliant init system and daemon process management utility set
6. A POSIX compliant bootloader
7. A POSIX compliant kernel
8. A kernel utilities set of your choice
9. An audio server of your choice
10. A package manager of your choice

This may not *seem* like much and that is on purpose. The whole point of DULOS is to only require the bare minimum so that you, the **actual user**, can decide what you want to use. There is no point in telling you that you need to use `ls` when no script **should be using ls**. Something like `ls` is an interactive tool that **you** use, so if you want to never install `ls` and just install something like `exa` instead that should be possible. So long as you have the required compliant software to build and run compliant programs, then you are good to go.

### What Are the Steps to Building the Base Operating System?
The steps to building the base operating system, while tedious and long, are not *too* complicated once you have done them once or twice. For the purposes of DULOS these steps are actually heavily trivialized down to running a few commands and then waiting for them to finish. To put it simply the steps to building the base operating system for DULOS are:

1. [Building GNU Guix (The Package Manager)](#building-gnu-guix-the-package-manager)
2. [Building the GNU GCC Toolchain](#building-the-gnu-gcc-toolchain)
3. [Building SBCL](#building-sbcl)
4. [Building sinit and daemonencore](#building-sinit-and-daemonencore)
5. [Building syslinux](#building-syslinux)
6. [Building linux](#building-linux)
7. [Building pipwire](#building-pipwire)
8. [Building busybox](#building-busybox)

## Building GNU Guix (The Package Manager)
The very first thing we will need to do is install our package manager, `guix`. `guix`, or GNU `guix`, is an advanced functional package manager written in Guile Scheme, a dialect of Lisp. This package fits perfectly into what DULOS wants to do as it is a programmable package manager, written in lisp, and does one thing and does it well.

In order to to install `guix` the first thing we will need to do is install a standard old version of it, then update said `guix` to the newest available version. To do the initial installation of `guix` we can use a script provided by the developers.
```
mkdir -p "${DULOS_BUILD}"/guix
cd "${DULOS_BUILD}"/guix
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
./guix-install
```
Answer yes when prompted. After the script finishes we will need to update `guix` to the latest version by running the following.
```
guix pull
```
This command may take some time so finish free to take a moment to walk around. Once the command finishes we should have an up-to-date version of `guix` which means we are ready to install the latest version of `guix` onto our target machine. The method we will be using to install `guix`, and much of our software, onto our system will be using `guix pack` to create a tar of our needed software so that we can manually install that onto our target system.
```
guix pack guix
```

## Building the GNU GCC Toolchain

## Buildig SBCL

## Building sinit and daemonencore

## Building syslinux

## Building linux

## Building busybox

## Building pipwire
