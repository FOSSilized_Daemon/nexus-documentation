# Table of Contents
* [Introduction](#introduction)
* [Determining Our System Drive](#determining-our-system-drive)
* [Formatting the System Drive](#formatting-the-system-drive)
* [Configuring the Physical Partition Scheme](#configuring-the-physical-partition-scheme)
* [Encrypting the System Drive](#encrypting-the-system-drive)
* [Configuring the Logical Volume Partition Scheme](#configuring-the-logical-volume-partition-scheme)
* [Installing the File System](#installing-the-file-system)
* [Mounting the System Disk Partitions and Creating the File System Hierarchy](#mounting-the-system-disk-partitions-and-creating-the-file-system-hierarchy)

## Introduction
This section will cover how to configure the system drive. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the following sub-section to begin!

### What is System Drive Configuration?
In this section, we will be configuring the *system drive* on which our operating system will be installed. This guide defines a system drive as any hard disc or solid-state drive that stores the base installation of an operating system.

*Read more about what a system drive is [here](insert-documentation-link-here).*

### What Are the Different Encryption Methods for the System Drive?
The topic of different drive encryption methods is often either quickly summarized or completely overlooked. This guide will provide a brief, but well-detailed, overview of the different types of drive configurations supported regarding encryption. While many methods of drive encryption exist, this guide will only focus on the following.

1. No drive encryption
2. Drive encryption with attached LUKS headers
3. Drive encryption with detached LUKS headers

The very first of the listed encryption methods, no encryption, is self-explanatory. While it may seem off to discuss no encryption as a method of encryption, it is listed here to acknowledge it as an option for drive security management rather than an actual drive encryption method. When using no encryption, the data on our drive is available to anyone with access to read the contents of the drive. Using no drive encryption means that no password is required to access the drive, nor is there any key needed to read the contents of your drive. Using no drive encryption is the least secure method of data storage and should never be used.

The second listed drive encryption method is full drive encryption with attached LUKS headers. Using the attached LUKS headers method means that all data on our drive, including all files, programs, the operating system itself, and everything in-between, will require a password at boot time to be read, written to, or executed. In other words, without the needed encryption password, *theoretically* no one will have access to the data stored on our drive. The actual LUKS keys, used to decrypt the drive when the valid password is given, are stored on an unencrypted part of the system, typically `/boot`. It is technically possible to encrypt the part of the system where these LUKS keys are stored; however, this tends to require special software such as the `GRUB2` bootloader. The attached LUKS headers drive encryption method is standard and used to achieve a reasonably secure system, though it does come with a downside. The downside of this data encryption method is that any skilled attacker can read the LUKS header file and determine what cipher was used to encrypt our drive, making brute-forcing the encryption much easier.

The third option of the encryption methods is to use full drive encryption with detached LUKS headers. This method is essentially the same as the last option, but the key difference is that the LUKS headers used to decrypt the disk are stored on a separate device, such as a USB flash drive, which has to be plugged into our computer in order to decrypt the drive. Detached LUKS headers are a two-factor authentication method for drive encryption used in high-security environments.

### What Are the Differences Between GPT and MBR?
While GPT and MBR are interesting topics to get into, and this sub-section will be updated to reflect that, at the moment all one needs to know is that the instructions for UEFI systems will use GPT and the instructions for BIOS systems will use MBR.

### What Are the Steps to Configuring the Drive?
The steps to configuring the system drive differ based on various circumstances. Attributes that impact the needed steps to configure a system drive properly are things such as whether the computer in use uses a [UEFI](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface) or [BIOS](https://en.wikipedia.org/wiki/BIOS) BIOS type and the style of encryption that is being used. However, to maintain a cleaner and easier-to-follow guide, the steps to configure the system drive have been streamlined to the following.

1. [Determining Our System Drive](#determining-our-system-drive)
2. [Formatting the System Drive](#formatting-the-system-drive)
3. [Configuring the Physical Partition Scheme](#configuring-the-physical-partition-scheme)
4. [Encrypting the System Drive](#encrypting-the-system-drive)
5. [Configuring the Logical Volume Partition Scheme](#configuring-the-logical-volume-partition-scheme)
6. [Installing the File System](#installing-the-file-system)
7. [Mounting the System Drive Partitions and Creating the File System Hierarchy](#mounting-the-system-drive-and-creating-the-file-system-hierarchy)

With that outlined, please proceed to the following sub-section: [Determining Our System Drive](#determining-our-system-drive).

## Determining Our System Drive
The first thing that we need to do is determine what our system drive is seen as by our operating system. The process of determining what our system drive is seen as is heavily dependent on whether we are using detached LUKS headers or not. We need to keep in mind our encryption method when checking for our system drive name because if we are using detached LUKS headers, we will need to check to see what our operating system sees our system drive as, along with what it sees our USB flash drive as well. Please be sure to follow the proper steps for your needs.

The easiest way to detect our system drive is to check for it with the `lsblk` and `grep` programs. To do this, we can check to see if any drives are found that start with *sd* or *nvme*. Do note that if one's drive is expected to be something other than *sd* or *nvme* one will need to make proper adjustments.
```
lsblk | grep 'sd\|nvme'
```
Then, if we plan to use detached LUKS headers, we need to find our USB flash drive by using the `lsblk` and `grep` programs to check if a drive named *sda* or *sdb* is found. Do note that if one's drive is expected to be something other than *sd*, one must make proper adjustments.
```
lsblk | grep 'sd'
```
With our drive names known, let us move on to the following sub-section: [Formatting the System Drive](#formatting-the-system-drive).

## Formatting the System Drive
In this sub-section, we will be formatting the system drive. One should note that the formatting process will completely erase all data currently on one's target drive; therefore, if there is anything that one needs to back up, one should do so before continuing with this guide. As with the above sub-section, the steps to be taken here are entirely dependent upon whether one is using detached LUKS headers or not. Please be sure to follow the proper steps for one's needs.

This guide will use the `gdisk` and `fdisk` programs for drive formatting and general management. The former program is used for managing drives with a GPT structure and the latter for drives with an MBR structure. GPT vs. MBR is a whole sub-section in and of itself, but the basics are if one's computer uses the classic BIOS boot, one should use MBR, whereas if one's computer uses the modern UEFI boot, one should use GPT.

To format a BIOS boot system's drive, use the following command.
```
fdisk /dev/sda
o
w
```
In contrast, use the following command to format a UEFI boot system's drive.
```
gdisk /dev/sda
o
w
```
Then, if one uses detached LUKS headers, one must format one's USB flash drive as follows.
```
fdisk /dev/sdb
o
w
```
With our system drive now formatted, move on to the following sub-section: [Configuring the Physical Partition Scheme](#configuring-the-physical-partition-scheme).

## Configuring the Physical Partition Scheme
Now that we have finished formatting our system drive and, if applicable, USB flash drive, it is time to create the physical partition scheme. A physical partition scheme is not easily adjustable, but that is fine, as we will create a logical partition scheme later. This step will depend on whether one is using detached LUKS headers or not, so please follow the appropriate steps for one's needs.

The process of creating the physical drive partition scheme is a simple and easy one that takes only one step. If our current computer uses BIOS boot and no drive encryption *or* attached LUKS headers, we will create the first physical partition to house our `/boot` directory, flagged as *bootable*, using the `fdisk` program.
```
fdisk /dev/sda
n
p
<ENTER>
<ENTER>
+512M
w
```
Otherwise, if we are using detached LUKS headers will, we will format our USB flash drive to hold the directories mentioned above.
```
fdisk /dev/sdb
n
p
<ENTER>
<ENTER>
<ENTER>
a
w
```
Finally, we will create our second or first physical partition to house the rest of our operating system.
```
fdisk /dev/sda
```
If one is not using detached LUKS headers, then tell `fdisk` to add a new partition.
```
a
```
Regardless of the type of drive encryption one uses, run the following commands.
```
n
p
<ENTER>
<ENTER>
<ENTER>
t
2
8e
w
```
In contrast, if we are on a computer that uses UEFI boot no drive encryption *or* attached LUKS headers, we will create the first two physical partitions to house our `/boot/efi` and `/boot` directories, flagged as *bootable*, using the `gdisk` program.
```
gdisk /dev/sda
n
p
+100M
a
n
p
<ENTER>
<ENTER>
+512M
```
Otherwise, if we are using detached LUKS headers will, we will format our USB flash drive to hold the directories mentioned above.
```
gdisk /dev/sdb
n
p
+100M
a
n
p
<ENTER>
<ENTER>
+512M
```
Finally, we will create our third or first physical partition to house the rest of our operating system.
```
gdisk /dev/sda
```
If one is not using detached LUKS headers, then tell `gdisk` to add a new partition.
```
a
```
Regardless of the type of drive encryption one uses, run the following commands.
```
n
p
<ENTER>
<ENTER>
<ENTER>
t
2
8e
w
```
With the physical partition scheme created, we are ready to move on to the following sub-section: [Encrypting the System Drive](#encrypting-the-system-drive).

## Encrypting the System Drive
System drive encryption is one of the most important steps when installing a new operating system. Drive encryption provides security and lets us know that someone cannot just boot off of a USB flash drive and start reading files from our computer. In this sub-section, we will encrypt our system drive with either attached or detached LUKS headers. Those who wish not to use drive encryption, **which is the most insecure thing one can do**, should skip this sub-section and move on to the next one, [Configuring the Logical Volume Partition Scheme](#configuring-the-logical-volume-partition-scheme).

We will use the `cryptsetup` program to encrypt the system drive. One should note that these steps heavily depend on one's system needs and thus should select and follow the proper instructions for one's requirements.

* [Encrypting the System Drive (Attached LUKS Headers)](#encrypting-the-system-drive-with-attached-luks-headers)
* [Encrypting the System Drive (Detached LUKS Headers)](#encrypting-the-system-drive-with-detached-luks-headers)

### Encrypting the System Drive (Attached LUKS Headers)
Encrypting the system drive is a simple process. To encrypt our system drive, we must use the `cryptsetup` program to encrypt the system with a *serpent* cipher and then open the encrypted drive. If our computer uses BIOS boot, then to encrypt and open our system drive, all we have to do is run the following commands. One should note that the password we type in when opening our encrypted drive during the first command will be our decryption password.
```
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/sda2
cryptsetup luksOpen /dev/sda2 cryptroot
```
In contrast, if our computer uses UEFI boot, then to encrypt and open our system drive, all we have to do is run the following commands. Again one should note that the password we type in when opening our encrypted drive during the first command will be our decryption password.
```
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/sda3
cryptsetup luksOpen /dev/sda3 cryptroot
```
Fantastic, now move on to the next sub-section: [Configuring the Logical Volume Partition Scheme](#configuring-the-logical-volume-partition-scheme).

### Encrypting the System Drive (Detached LUKS Headers)
Encrypting the system drive is a simple process. To encrypt our system drive, we must use the `cryptsetup` program to encrypt the USB flash drive with a *serpent* cipher, create a file system on the USB flash drive, mount the USB flash drive, open the encrypted USB flash drive, encrypt the system drive using the LUKS headers on the USB flash drive, and then finally open the encrypted system disk.

If our computer uses BIOS boot, we must run the following command to encrypt the USB flash drive.
```
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 500 --use-random luksFormat /dev/sdb1
```
In contrast, if our computer uses UEFI boot, we must run the following command to encrypt the USB flash drive.
```
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 500 --use-random luksFormat /dev/sdb2
```
Now we can open the newly encrypted system using `cryptsetup`. One should note that the password one types here will be one's decryption password. To open the encrypted system on a BIOS boot computer, use the following command.
```
cryptsetup luksOpen /dev/sdb1 cryptboot
```
To open the encrypted system on a UEFI boot computer, use the following command. As previously stated, one should note that the password one types here will be one's decryption password.
```
cryptsetup luksOpen /dev/sdb2 cryptboot
```
Next, we need to create the file system on our USB flash drive and mount it. If one is on a BIOS boot computer, one should run the following commands to create an *ext4* file system on the USB flash drive and then mount it.
```
mkfs.ext4 -L boot /dev/mapper/cryptboot
mkdir /mnt/usb
mount /dev/mapper/cryptboot /mnt/usb
```
In contrast, if our computer uses UEFI boot, we should run the following commands instead.
```
mkfs.vfat -F 32 -n EFI /dev/sdb1
mkfs.ext4 /dev/mapper/cryptboot
mkdir /mnt/usb
mount /dev/mapper/cryptboot /mnt/usb
```
Next, encrypt the system disk using the detached LUKS headers by running the following commands.
```
truncate -s 4M /mnt/usb/luksheader
cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/sda1 --align-payload 4096 --header /mnt/usb/luksheader
cryptsetup open --type luks --header /mnt/usb/luksheader /dev/sda1 cryptroot
```
Finally, we need to unmount the USB flash drive.
```
unmount /mnt/usb
```
Fantastic, now move on to the next sub-section: [Configuring the Logical Volume Partition Scheme](#configuring-the-logical-volume-partition-scheme).

## Configuring the Logical Volume Partition Scheme
Now that our system disk is encrypted, it is time for us to create our logical volume partition scheme. Logical volumes allow us to create disk partitions that can be easily resized, stripped, and mirrored. Using logical volumes, we can create a partition scheme ideal for long-term and secure use on our disk. Creating a logical volume partition scheme is simple and involves the following steps.

1. [Creating the Physical Volume and Volume Group On Our Encrypted Pool](#creating-the-physical-volume-and-volume-group-on-our-encrypted-pool)
2. [Creating the Logical Volumes on Our Volume Group](#creating-the-logical-volumes-on-our-volume-group)

### Creating the Physical Volume and Volume Group On Our Encrypted Pool
Before we can create our logical volumes, we must first create the physical volume and volume group on our encrypted pool. We need to run the following commands if we are using full drive encryption.
```
pvcreate /dev/mapper/cryptroot
vgcreate uloslvm /dev/mapper/cryptroot
```
Otherwise, if we are not using full drive encryption, we need to run the below commands.
```
pvcreate /dev/mapper/sda2
vgcreate uloslvm /dev/mapper/sda2
```
Fantastic, now move on to the next sub-section: [Creating the Logical Volumes on Our Volume Group](#creating-the-logical-volumes-on-our-volume-group).

### Creating the Logical Volumes on Our Volume Group
With our physical volume and volume group created on our encrypted pool, it is finally time to create our logical volume partition scheme. Please note that the following sizes are mock and not the exact size one should use. These sizes assume one has a drive of 500GB.
```
lvcreate --name root -L 5G uloslvm
lvcreate --name gnu -L 50G uloslvm
lvcreate --name usr -L 25G uloslvm
lvcreate --name opt -L 10G uloslvm
lvcreate --name var -L 25G uloslvm
lvcreate --name tmp -L 5G uloslvm
lvcreate --name vartmp -L 10G uloslvm
lvcreate --name home -l 100%FREE uloslvm
```
Fantastic, now move on to the next sub-section: [Installing the File System](#installing-the-file-system).

## Installing the File System
Now that we have finished creating our partitions, it is time to install the file systems onto the drive partitions. Installing the file system onto our partitions is a relatively simple process. We will use the `mkfs.ext4` to install the *ext4* file system onto our partitions. We should use the following commands if our computer uses attached LUKS headers and BIOS boot.
```
mkfs.ext4 -L boot /dev/sda1
mkfs.ext4 -L root /dev/uloslvm/root
mkfs.ext4 -L gnu /dev/uloslvm/gnu
mkfs.ext4 -L home /dev/uloslvm/home
mkfs.ext4 -L usr /dev/uloslvm/usr
mkfs.ext4 -L opt /dev/uloslvm/opt
mkfs.ext4 -L var /dev/uloslvm/var
mkfs.ext4 -L tmp /dev/uloslvm/tmp
mkfs.ext4 -L vartmp /dev/uloslvm/vartmp
```
In contrast, If our computer uses attached LUKS headers and UEFI boot, we should use the below commands which will also include the steps to create the *FAT32* file system on our USB flash drive, using `mkfs.vfat`.
```
mkfs.ext4 -L boot /dev/sda2
mkfs.vfat -F 32 -n EFI /dev/sda1
mkfs.ext4 -L root /dev/uloslvm/root
mkfs.ext4 -L gnu /dev/uloslvm/gnu
mkfs.ext4 -L home /dev/uloslvm/home
mkfs.ext4 -L usr /dev/uloslvm/usr
mkfs.ext4 -L opt /dev/uloslvm/opt
mkfs.ext4 -L var /dev/uloslvm/var
mkfs.ext4 -L tmp /dev/uloslvm/tmp
mkfs.ext4 -L vartmp /dev/uloslvm/vartmp
```
Otherwise, if our computer configuration uses detached LUKS headers, we should use the commands below.
```
mkfs.ext4 -L root /dev/uloslvm/root
mkfs.ext4 -L gnu /dev/uloslvm/gnu
mkfs.ext4 -L home /dev/uloslvm/home
mkfs.ext4 -L usr /dev/uloslvm/usr
mkfs.ext4 -L opt /dev/uloslvm/opt
mkfs.ext4 -L var /dev/uloslvm/var
mkfs.ext4 -L tmp /dev/uloslvm/tmp
mkfs.ext4 -L vartmp /dev/uloslvm/vartmp
```
Fantastic, now move on to the next sub-section: [Mounting the System Disk Partitions and Creating the File System Hierarchy](#mounting-the-system-disk-and-creating-the-file-system-hierarchy).

## Mounting the System Disk Partitions and Creating the File System Hierarchy
The file system hierarchy is one of the most critical aspects of an operating system as it is the cornerstone of how things on an operating system are organized. In the GNU/Linux sphere, sadly, file system hierarchies are extremely dirty and cluttered. It is common for things to be placed randomly in directories they do not belong in, and no one seems interested in fixing these issues. ULOS, on the other hand, aims to use a clean and well-constructed file system hierarchy, and it will be further cleaned as software is written specifically for ULOS. The mounting and creating of the file system hierarchy is quick and easy, taking only one step.

* [Mounting the System Disk Partitions and Creating the File System Hierarchy](#mounting-the-system-disk-and-creating-the-file-system-hierarchy)

### Mounting the System Disk Partitions and Creating the File System Hierarchy
The very first thing we need to do is mount our root partition to the root directory.
```
mount /dev/uloslvm/root "${ULOS_ROOT}"
```
We need to create the few directories we will need to mount the rest of our partitions.
```
mkdir "${ULOS_ROOT}"/boot
mkdir "${ULOS_ROOT}"/gnu
mkdir "${ULOS_ROOT}"/home
mkdir "${ULOS_ROOT}"/usr
mkdir "${ULOS_ROOT}"/opt
mkdir "${ULOS_ROOT}"/var
mkdir "${ULOS_ROOT}"/tmp
mkdir "${ULOS_ROOT}"/var/tmp
```
Now we can mount all the rest of the partitions. If our computer uses attached LUKS headers and BIOS boot, we need to run the below commands to mount all of our partitions.
```
mount /dev/sda1 "${ULOS_ROOT}"/boot
mount /dev/uloslvm/gnu "${ULOS_ROOT}"/gnu
mount /dev/uloslvm/home "${ULOS_ROOT}"/home
mount /dev/uloslvm/usr "${ULOS_ROOT}"/usr
mount /dev/uloslvm/opt "${ULOS_ROOT}"/opt
mount /dev/uloslvm/var "${ULOS_ROOT}"/var
mount /dev/uloslvm/tmp "${ULOS_ROOT}"/tmp
mount /dev/uloslvm/vartmp "${ULOS_ROOT}"/var/tmp
```
In contrast, if our computer uses attached LUKS headers and UEFI boot, we need to run the below commands to mount all of our partitions.
```
mount /dev/sda2 "${ULOS_ROOT}"/boot
mount /dev/uloslvm/gnu "${ULOS_ROOT}"/gnu
mount /dev/uloslvm/home "${ULOS_ROOT}"/home
mount /dev/uloslvm/usr "${ULOS_ROOT}"/usr
mount /dev/uloslvm/opt "${ULOS_ROOT}"/opt
mount /dev/uloslvm/var "${ULOS_ROOT}"/var
mount /dev/uloslvm/tmp "${ULOS_ROOT}"/tmp
mount /dev/uloslvm/vartmp "${ULOS_ROOT}"/var/tmp
```
Otherwise, if our computer uses a detached LUKS headers configuration and BIOS boot, we should run the below commands to mount all of our partitions.
```
mount /dev/sdb1 "${ULOS_ROOT}"/boot
mount /dev/uloslvm/gnu "${ULOS_ROOT}"/gnu
mount /dev/uloslvm/home "${ULOS_ROOT}"/home
mount /dev/uloslvm/usr "${ULOS_ROOT}"/usr
mount /dev/uloslvm/opt "${ULOS_ROOT}"/opt
mount /dev/uloslvm/var "${ULOS_ROOT}"/var
mount /dev/uloslvm/tmp "${ULOS_ROOT}"/tmp
mount /dev/uloslvm/vartmp "${ULOS_ROOT}"/var/tmp
```
In contrast, if our computer uses a detached LUKS headers configuration and UEFI boot, we should run the below commands to mount all of our partitions.
```
mount /dev/mapper/cryptboot "${ULOS_ROOT}"/boot
mount /dev/uloslvm/gnu "${ULOS_ROOT}"/gnu
mount /dev/uloslvm/home "${ULOS_ROOT}"/home
mount /dev/uloslvm/usr "${ULOS_ROOT}"/usr
mount /dev/uloslvm/opt "${ULOS_ROOT}"/opt
mount /dev/uloslvm/var "${ULOS_ROOT}"/var
mount /dev/uloslvm/tmp "${ULOS_ROOT}"/tmp
mount /dev/uloslvm/vartmp "${ULOS_ROOT}"/var/tmp
```
Next, we need to create the file system hierarchy on our target system using the [buildfshier](https://gitlab.com/FOSSilized_Daemon/nexus-scripts/-/blob/master/src/ULOS/section-01/buildfsheir) script.
```
cd "${ULOS_SCRIPTS}"/section-01
./buildfshier "${ULOS_ROOT}"
cd
```
If our computer uses UEFI boot, we must create the `/boot/efi` directory.
```
mount /dev/sdb1 "${ULOS_ROOT}"/boot/
mkdir "${ULOS_ROOT}"/boot/efi
```
Fantastic, now move on to the next section: [Building the Base Operating System](https://gitlab.com/FOSSilized_Daemon/nexus/-/blob/master/doc/guides/nexus-for-pcs/sections/old-sections/section-03_building-the-base-operating-system.md).
