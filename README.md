# Table of Contents
- [Introduction](#introduction)
	- [What is Nexus?](#what-is-nexus)
	- [The Rubber Hose Attack (No System is 100% Secure)](#the-rubber-hose-attack-no-system-is-100%-secure)
	- [Don't Be Evil](#dont-be-evil)
	- [Where to Start?](#where-to-start)
- [Nexus Guides](#nexus-versions)
	- [Nexus For PCs](#nexus-for-pcs)
	- [Nexus For Phones](#nexus-for-phones)
	- [Nexus For Tablets](#nexus-for-tablets)
- [Contribute](#contribute)
	- [Proposing Changes to Existing Guides](#proposing-changes-to-existing-guides)
	- [Requirements to Be a Contributor to Nexus](#requirements-to-be-a-contributor-to-nexus)
- [Additional Resources](#additional-resources)
	- [Privacy Resources](#privacy-resources)
	- [Hardware Resources](#hardware-resources)

## Introduction
Welcome to Nexus! In this README, one will find a brief overview of what Nexus is and what it offers. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the next section to begin!

### What is Nexus?
Nexus is a series of guides on building secure operating systems from scratch. Using [free and open-source software](https://en.wikipedia.org/wiki/Free_and_open-source_software), Nexus will walk one through how to build security and privacy-focused operating systems entirely from scratch. Nexus aims to help educate people on the possibility of personalized custom operating systems and extend what is typically considered a *"reasonably secure operating system"*.

While Nexus can be helpful to everyone, this customization level comes at a cost. Most people will not be able to use Nexus. It is highly recommended that anyone wishing to use Nexus be familiar with handbooks such as [Linux From Scratch](http://www.linuxfromscratch.org/lfs/read.html). Additionally, anyone wishing to use Nexus should be well versed in building and managing everything from an operating systems userland, more specifically on [UNIX-like operating systems](https://en.wikipedia.org/wiki/Unix-like) and [LISP machines](https://en.wikipedia.org/wiki/Lisp_machine), to an advanced firewall. It is correct to say that anyone *could* build an operating system using Nexus. However, it *should* be **heavily** understood that the resulting system would likely fall apart and be rendered unusable if the person who built it did not know how to maintain it.

### The Rubber Hose Attack (No System is 100% Secure)
Security is a very crucial part of Nexus. The philosophy of these guides is *that if something on an operating system can be secured, it should be*. This philosophy often leaves users with the idea that their system is fool-proof, but this is **never** the case. Readers must understand: **Nothing is 100% secure. No system is hack-proof. No one is immune to a rubber hose attack.** If one is doing something that draws someone's attention and they want access to one's system, detached LUKS headers will not stop them. Having a strong password will not keep an attacker out. If all else fails, they will **always** fall back to the [rubber hose attack](https://en.wikipedia.org/wiki/Rubber-hose_cryptanalysis). If one has data that is *so* sensitive, there can't be any chance of someone else getting their hands on it; most security experts recommend that said data never touch a computer or network. On the other hand, Nexus suggests that said data not exist. Everything, whether it be written on a piece of paper or a disk, is obtainable to an attacker. The best we can do is have good [OpSec](https://en.wikipedia.org/wiki/Operations_security) and try to ensure we can keep most people out. It should be noted, **heavily**, that Nexus is not ensuring 100% security but does try to secure whatever it can.

### Don't Be Evil
Throughout Nexus, one will learn how to configure certain types of technologies that *can* be used to do illegal and immoral things. Noting this, we feel it necessary to add this warning. **Don't. Be. Evil.** If one intends to use these guides for such activities, there is little to nothing that we can do to stop them. But, know what we discussed in the prior sub-section: **Nothing one does is 100% safe.** If one is doing... things, which we feel no need to name, one will eventually be caught. No amount of encryption or fail-safes can cover the fact that, over one's lifetime, one will **eventually** fuck up.

### Where to Start?
Where one decides to start is entirely depending on one's needs. If one wishes to reset one's prior activities and engagements completely, it is recommended that one see the [Additional Resources](#additional-resources) section. The aforementioned section provides worthwhile projects for removing the data collected on oneself, or what data that can be removed, and good places to acquire safe hardware. Then, move on to the [Nexus Guides](#nexus-guides) section and build a custom and secure system for one's device.

## Nexus Guides
This section holds a master list of the different guides provided by Nexus.

### Nexus For PCs
Those wishing to build an operating system for laptops or desktops should use the [Nexus For PCs](doc/guides/nexus-for-pcs/nexus-for-pcs-introduction.md) guide. In this guide, one will build an operating system called *ULOS*, or the **U**NIX and **L**isp-like **O**perting **S**ystem, for everyday laptop or desktop use.

## Contribute
Due to the sheer amount of information covered in these guides, there will surely be errors or things that could be improved upon over time. This section covers the guidelines for contributing to Nexus.

### Proposing Changes to Existing Guides
If one wishes to change an existing guide or document, one must make a [pull request](https://docs.gitlab.com/ee/topics/gitlab_flow.html#mergepull-requests-with-gitlab-flow) on this repository with the desired changes and a detailed explanation of what the differences are and why they are needed. If new technologies are introduced, one will be required to link helpful documentation and resources for the said technologies.

### Requirements to Be a Contributor to Nexus
Anyone wishing to contribute to Nexus must allow their contributions to be licensed under the AGPL. Additionally, all contributors **must** follow the [Nexus Code of Conduct]().

## Additional Resources
This section links valuable resources that we feel will help one configure and secure one's systems better. While some of these links may not feel relevant, we assure one that they help improve one's privacy and security.

### Privacy Resources
* [Have I Been Pwned](https://haveibeenpwned.com/)
* [Intel Techniques Resources](https://inteltechniques.com/links.html)

### Hardware Resources
* [Linux Hardware Database](https://linux-hardware.org/)
* [Linux on Laptops](https://linux-laptop.net/)
* [Framework](https://frame.work/)
* [System76](https://system76.com/)
* [Free Software Foundation: Hardware Devices](https://www.fsf.org/resources/hw)
